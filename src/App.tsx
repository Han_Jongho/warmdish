import React, { useEffect, useState } from 'react';
import './App.css';

import Panel from './panel';

let posX = 0;
const defaultWidth = 300;
const minimumWidth = 100;
const splitWidth = 40;

let preWidth1 = defaultWidth, preWidth2 = defaultWidth;

function App() {

  const [size, setSize] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  useEffect(() => {
    window.addEventListener('dragover', (e) => {
      e.preventDefault();
    });
    window.addEventListener('drop', (e) => {
      e.preventDefault();
    });
  })

  const [width1, setWidth1] = useState(defaultWidth);
  const [width2, setWidth2] = useState(defaultWidth);

  const onDragStart1 = (e: React.DragEvent<HTMLDivElement>) => {
    posX = e.clientX;
  }

  const onDrag1 = (e: React.DragEvent<HTMLDivElement>) => {
    const newWidth = width1 + e.clientX - posX;
    if (size.width - splitWidth - width2 - newWidth < minimumWidth) {
      const maxWidth = size.width - minimumWidth - splitWidth - width2;
      preWidth1 = maxWidth;
      setWidth1(maxWidth);
    }
    else if (newWidth > minimumWidth) {
      posX = e.clientX;
      preWidth1 = newWidth;
      setWidth1(newWidth);
    }
    else {
      preWidth1 = minimumWidth;
      setWidth1(minimumWidth);
    }
  }

  const onDragEnd1 = (e: React.DragEvent<HTMLDivElement>) => {
    const newWidth = width1 + e.clientX - posX;
    if (size.width - splitWidth - width2 - newWidth < minimumWidth) {
      const maxWidth = size.width - minimumWidth - splitWidth - width2;
      preWidth1 = maxWidth;
      setWidth1(maxWidth);
    }
    else if (newWidth > minimumWidth) {
      preWidth1 = newWidth;
      setWidth1(newWidth);
    }
    else {
      preWidth1 = minimumWidth;
      setWidth1(minimumWidth);
    }
  }

  const onDragStart2 = (e: React.DragEvent<HTMLDivElement>) => {
    posX = e.clientX;
  }

  const onDrag2 = (e: React.DragEvent<HTMLDivElement>) => {
    const newWidth = width2 - e.clientX + posX;
    if (size.width - splitWidth - width1 - newWidth < minimumWidth) {
      const maxWidth = size.width - minimumWidth - splitWidth - width1;
      preWidth2 = maxWidth;
      setWidth2(maxWidth);
    }
    else if (newWidth > minimumWidth) {
      posX = e.clientX;
      preWidth2 = newWidth;
      setWidth2(newWidth);
    }
    else {
      preWidth2 = minimumWidth;
      setWidth2(minimumWidth);
    }
  }

  const onDragEnd2 = (e: React.DragEvent<HTMLDivElement>) => {
    const newWidth = width2 - e.clientX + posX;
    if (size.width - splitWidth - width1 - newWidth < minimumWidth) {
      const maxWidth = size.width - minimumWidth - splitWidth - width1;
      preWidth2 = maxWidth;
      setWidth2(maxWidth);
    }
    else if (newWidth > minimumWidth) {
      preWidth2 = newWidth;
      setWidth2(newWidth);
    }
    else {
      preWidth2 = minimumWidth;
      setWidth2(minimumWidth);
    }
  }

  useEffect(() => {
    const handleResize = () => {
      if (window.outerWidth < 400) {
        window.resizeTo(400, window.outerHeight);
      }

      if (window.innerWidth - splitWidth - width1 - width2 < minimumWidth) {
        const newWidth1 = window.innerWidth - splitWidth - width2 - minimumWidth;
        if (newWidth1 < minimumWidth) {
          setWidth1(minimumWidth);
          const newWidth2 = width2 - minimumWidth + newWidth1;
          setWidth2(newWidth2);
        }
        else
          setWidth1(newWidth1);
      }
      else {
        const delta = window.innerWidth - splitWidth - width1 - width2 - minimumWidth;

        if (preWidth2 > width2) {
          setWidth2(width2 + delta);
        }
        else if (preWidth2 < width2) {
          setWidth2(preWidth2);
        }
        else if (preWidth1 > width1) {
          setWidth1(width1 + delta);
        }
        else if (preWidth1 < width1) {
          setWidth1(preWidth1);
        }
      }

      setSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    }
  });

  const innerWidth = size.width - width1 - width2 - splitWidth;
  return (
    <div className="app">
      <Panel width={width1} height={window.innerHeight} color="blue"/>
      <div className="drag" onDragStart={onDragStart1} onDrag={onDrag1} onDragEnd={onDragEnd1}></div>
      <Panel width={innerWidth} height={window.innerHeight} color="red"/>
      <div className="drag" onDragStart={onDragStart2} onDrag={onDrag2} onDragEnd={onDragEnd2}></div>
      <Panel width={width2} height={window.innerHeight} color="yellow"/>
    </div>
  );
}

export default App;
