import React from 'react';

type PanelProps = {
    width: number;
    height: number;
    color: string;
}

function Panel({width, height, color}: PanelProps) {
  const style = {
      width: `${width}px`,
      height: `${height}px`,
      background: color,
  };

  return (
    <div style={style} >{`width: ${width}, height: ${height}`}</div>
  );
}

export default Panel;
